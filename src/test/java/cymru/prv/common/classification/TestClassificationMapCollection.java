package cymru.prv.common.classification;

import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

public class TestClassificationMapCollection {

    @Test
    public void testAddData() {
        var collection = new ClassificationMapCollection<String, String>();

        // Ensure the collection is in-fact empty
        assertEquals(0, collection.getKeys().size());

        // Insert data
        collection.increment("results", "hello", "hello");

        // Check that a new classification map was created
        assertEquals(1, collection.getKeys().size());
        assertTrue(collection.hasKey("results"));
        var map = collection.getClassificationMap("results");

        // Check that the function returns a list with the given table
        Collection<ClassificationMap<String>> col = Collections.singletonList(map);
        assertIterableEquals(col, collection.getClassificationMaps());

        // Check if the data was updated in the correct map
        assertEquals(1, map.getData("hello", "hello"));
        assertEquals(0, map.getData("world", "hello"));
        assertEquals(0, map.getData("hello", "world"));
    }

}
