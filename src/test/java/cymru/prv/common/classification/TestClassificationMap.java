package cymru.prv.common.classification;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestClassificationMap {

    @Test
    public void testIncreaseNumber(){
        var map = new ClassificationMap<String>();
        map.increment("hello", "hello");
        assertEquals(1, map.getData("hello", "hello"));
        assertEquals(0, map.getData("world", "hello"));
        assertEquals(0, map.getData("hello", "world"));
    }

    @Test
    public void testIncreaseWrongClassification(){
        var map = new ClassificationMap<String>();
        map.increment("hello", "world");
        assertEquals(1, map.getData("hello", "world"));
        assertEquals(0, map.getData("hello", "hello"));
        assertEquals(0, map.getData("world", "world"));

    }

}
