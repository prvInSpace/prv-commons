package cymru.prv.common.cli;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestConsoleInterface {

    private void openStdIn(String expected){
        var line = ConsoleInterface.get().nextLine();
        assertEquals(expected, line);
    }

    @Test
    public void testOpenStreamTwoTimes() {
        ConsoleInterface.get().addData("hello\nworld!\n");
        openStdIn("hello");
        openStdIn("world!");
    }

    @Test
    public void testReadFromDifferentSources() {
        System.setIn(new ByteArrayInputStream("cruel".getBytes()));
        ConsoleInterface.get().addData("hello");
        openStdIn("hello");
        openStdIn("cruel");
        ConsoleInterface.get().addData("world!");
        openStdIn("world!");
    }

    @Test
    public void testSetShouldOverrideData(){
        ConsoleInterface.get().addData("hello");
        ConsoleInterface.get().setData("world");
        assertEquals("world", ConsoleInterface.get().nextLine());
    }

}
