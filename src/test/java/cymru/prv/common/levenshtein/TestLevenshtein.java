package cymru.prv.common.levenshtein;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class TestLevenshtein {

    @Test
    public void testUnchangedString(){
        String[] list1 = new String[]{
                "hello",
                "world"
        };
        String[] list2 = new String[]{
                "hello",
                "world"
        };
        var result = new LevenshteinDistance().difference(list1, list2);
        Assertions.assertEquals(0, result.getDistance());
        Assertions.assertIterableEquals(List.of(LevenshteinAction.NOTHING, LevenshteinAction.NOTHING), result.getActions());
        Assertions.assertIterableEquals(
                List.of("hello", "world"),
                result.getCommon()
        );
    }

    @Test
    public void testMissingWord(){
        String[] list1 = new String[]{
                "hello",
                "cruel",
                "world"
        };
        String[] list2 = new String[]{
                "hello",
                "world"
        };
        var result = new LevenshteinDistance().difference(list1, list2);
        Assertions.assertFalse(result.toString().isBlank());
        Assertions.assertEquals(1, result.getDistance());
        Assertions.assertIterableEquals(
                List.of(LevenshteinAction.NOTHING, LevenshteinAction.INSERTION, LevenshteinAction.NOTHING),
                result.getActions()
        );
        Assertions.assertIterableEquals(
                List.of("hello", "world"),
                result.getCommon()
        );
    }

    @Test
    public void testAdditionalWord(){
        String[] list1 = new String[]{
                "hello",
                "world"
        };
        String[] list2 = new String[]{
                "hello",
                "cruel",
                "world"
        };
        var result = new LevenshteinDistance().difference(list1, list2);
        Assertions.assertFalse(result.toString().isBlank());
        Assertions.assertEquals(1, result.getDistance());
        Assertions.assertIterableEquals(
                List.of(LevenshteinAction.NOTHING, LevenshteinAction.DELETION, LevenshteinAction.NOTHING),
                result.getActions()
        );
        Assertions.assertIterableEquals(
                List.of("hello", "world"),
                result.getCommon()
        );
    }


    @Test
    public void testSubstitute(){
        String[] list1 = new String[]{
                "hello",
                "happy",
                "world"
        };
        String[] list2 = new String[]{
                "hello",
                "cruel",
                "world"
        };
        var result = new LevenshteinDistance().difference(list1, list2);
        Assertions.assertFalse(result.toString().isBlank());
        Assertions.assertEquals(1, result.getDistance());
        Assertions.assertIterableEquals(
                List.of(LevenshteinAction.NOTHING, LevenshteinAction.SUBSTITUTION, LevenshteinAction.NOTHING),
                result.getActions()
        );
        Assertions.assertIterableEquals(
                List.of("hello", "world"),
                result.getCommon()
        );
    }

    @Test
    public void testOnlyInsertions() {
        String[] list1 = new String[]{
                "hello",
                "happy",
                "world"
        };
        String[] list2 = new String[]{};
        var result = new LevenshteinDistance().difference(list1, list2);
        Assertions.assertFalse(result.toString().isBlank());
        Assertions.assertEquals(3, result.getDistance());
        Assertions.assertIterableEquals(
                List.of(LevenshteinAction.INSERTION, LevenshteinAction.INSERTION, LevenshteinAction.INSERTION),
                result.getActions()
        );
        Assertions.assertIterableEquals(
                List.of(),
                result.getCommon()
        );
    }

    @Test
    public void testOnlyDeletions() {
        String[] list1 = new String[]{};
        String[] list2 = new String[]{
                "hello",
                "happy",
                "world"
        };
        var result = new LevenshteinDistance().difference(list1, list2);
        Assertions.assertFalse(result.toString().isBlank());
        Assertions.assertEquals(3, result.getDistance());
        Assertions.assertIterableEquals(
                List.of(LevenshteinAction.DELETION, LevenshteinAction.DELETION, LevenshteinAction.DELETION),
                result.getActions()
        );
        Assertions.assertIterableEquals(
                List.of(),
                result.getCommon()
        );
    }

    @Test
    public void testSubstitutionScoreHigherThan2ShouldNotSubstitute() {
        String[] list1 = new String[]{
                "hello",
                "cruel",
                "world"
        };
        String[] list2 = new String[]{
                "hello",
                "happy",
                "world"
        };
        var result = new LevenshteinDistance(3, 1, 1).difference(list1, list2);
        Assertions.assertFalse(result.toString().isBlank());
        Assertions.assertEquals(2, result.getDistance());
        Assertions.assertIterableEquals(
                List.of(LevenshteinAction.NOTHING, LevenshteinAction.INSERTION, LevenshteinAction.DELETION, LevenshteinAction.NOTHING),
                result.getActions()
        );
        Assertions.assertIterableEquals(
                List.of("hello", "world"),
                result.getCommon()
        );
    }
}
