package cymru.prv.common.threading;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicBoolean;

public class TestTaskSequentializer {

    @Test
    public void testNormalOpenAndClose() throws InterruptedException {
        var handler = new TaskSequentializer();
        AtomicBoolean first = new AtomicBoolean(false);
        AtomicBoolean second = new AtomicBoolean(false);

        handler.addTask(() -> first.set(true));
        handler.addTask(() -> second.set(true));

        handler.finishAndClose();
        Assertions.assertTrue(first.get());
        Assertions.assertTrue(second.get());
    }

    @Test
    public void testTryWithResources() throws InterruptedException {

        try(var handler = new TaskSequentializer()){

            // Set up an never ending series of tasks
            Runnable[] runnables = new Runnable[1];
            runnables[0] = () -> {
                try {
                    Thread.sleep(10);
                } catch (InterruptedException ignored){}
                handler.addTask(runnables[0]);
            };
            handler.addTask(runnables[0]);

            Thread.sleep(50);

            // The thread should still be handling threads when we finish
            Assertions.assertNotEquals(Thread.State.WAITING, handler.getState());
        }

        // It should close normally and not throw and exception
    }

    @Test
    public void testShouldExistIfThreadIsInterrupted() throws InterruptedException {
        var handler = new TaskSequentializer();
        handler.addTask(() -> {
            Thread.currentThread().interrupt();
        });
        Thread.sleep(25);

        // The thread should be terminated by the call to interrupt
        Assertions.assertEquals(Thread.State.TERMINATED, handler.getState());
        handler.close();
    }

}
