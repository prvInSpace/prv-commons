package cymru.prv.common.threading;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;


/**
 * A class which helps delegate tasks to a single thread.
 * Helpful in multi-threaded environments where some tasks
 * need to be performed sequentially.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class TaskSequentializer implements AutoCloseable {

    private final ConcurrentLinkedQueue<Runnable> queue = new ConcurrentLinkedQueue<>();
    private final AtomicBoolean running = new AtomicBoolean(false);
    private final Thread thread;

    /**
     * Creates a new instance of the object
     * Sets up a new thread to handle the tasks
     * and starts it.
     */
    public TaskSequentializer() {
        thread = new Thread(this::threadMethod);
        thread.setName("Single Thread Handler");
        thread.start();
    }


    /**
     * Sets the running flag to false
     * and waits for the task thread to
     * finished the rest of the queue.
     *
     * @throws InterruptedException if the current thread gets interrupted while waiting
     */
    public void finishAndClose() throws InterruptedException {
        running.set(false);
        synchronized (queue) {
            queue.notifyAll();
        }
        thread.join();
    }


    /**
     * Interrupts the main thread and
     * joins it with the current thread.
     * That should be enough to kill it.
     */
    @Override
    public void close() throws InterruptedException {
        running.set(false);
        thread.interrupt();
        thread.join(1000);
    }


    /**
     * Adds a task to the queue and notifies
     * the running thread if needed.
     *
     * @param runnable the task to add
     */
    public void addTask(Runnable runnable) {
        queue.add(runnable);
        new Thread(() -> {
            synchronized (queue){
                queue.notifyAll();
            }
        }).start();
    }

    private void threadMethod() {
        running.set(true);
        synchronized (queue) {
            try {
                while (!Thread.currentThread().isInterrupted()) {
                    // Check if the queue is empty
                    if (queue.isEmpty()) {
                        if (!running.get())
                            return;
                        else {
                            synchronized (queue) {
                                queue.wait();
                            }
                        }
                    }

                    // Otherwise pop the next task
                    // and perform it.
                    else
                        queue.poll().run();
                }
            } catch (InterruptedException ignored) {}
        }
    }


    /**
     * Return the state of the thread.
     * Used for testing purposes
     *
     * @return the state of the thread.
     */
    Thread.State getState(){
        return thread.getState();
    }

}
