package cymru.prv.common.cli;

import java.util.Arrays;
import java.util.Queue;
import java.util.Scanner;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * <p>
 * A class to handle input from System.in while
 * also providing functionality to inject data
 * for use in testing.
 * </p>
 * <p>
 * This is useful since only because it prevents
 * System.in from getting closed while still allowing
 * functions that require input to be tested.
 * </p>
 * @author Preben Vangberg
 * @since 1.0.0
 */
public final class ConsoleInterface {

    private static ConsoleInterface INSTANCE;
    private final Scanner stdin = new Scanner(System.in);
    private final Queue<String> stringQueue = new LinkedBlockingQueue<>();

    private ConsoleInterface(){}


    /**
     * Returns the next line.
     * If there is lines in the queue
     * it will return that, otherwise it
     * will return the next line from
     * System.in
     *
     * @return the next line of input
     */
    public String nextLine(){
        if(!stringQueue.isEmpty())
            return stringQueue.poll();
        return stdin.nextLine();
    }


    /**
     * Returns the instance of the ConsoleInterface
     *
     * @return the instance of the ConsoleInterface
     */
    public static ConsoleInterface get(){
        if(INSTANCE == null)
            INSTANCE = new ConsoleInterface();
        return INSTANCE;
    }


    /**
     * Adds strings to the queue before the
     * next input from System.in
     *
     * @param data the strings to add
     */
    public void addData(String data){
        String[] strings = data.split("\r?\n");
        stringQueue.addAll(Arrays.asList(strings));
    }


    /**
     * Clears the strings in the current instance
     * and adds strings to the queue before the
     * next input from System.in
     *
     * @param data the strings to add
     */
    public void setData(String data){
        clear();
        addData(data);
    }


    /**
     * Clears the queue for all strings
     */
    public void clear(){
        stringQueue.clear();
    }
}
