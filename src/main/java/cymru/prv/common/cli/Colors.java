package cymru.prv.common.cli;


/**
 * A list of several common colour codes that can be
 * used for colouring text.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public final class Colors {

    private Colors(){}

    public static final String RESET = "\u001B[0m";
    public static final String GREEN = "\u001B[32m";
    public static final String YELLOW = "\u001B[33m";
    public static final String RED = "\u001B[31m";

}
