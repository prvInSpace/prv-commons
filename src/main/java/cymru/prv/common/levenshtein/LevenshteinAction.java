package cymru.prv.common.levenshtein;


/**
 * An enum class representing the actions
 * in which the levenshtein algorithm may
 * take
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public enum LevenshteinAction {
    INSERTION, DELETION, SUBSTITUTION, NOTHING
}
