package cymru.prv.common.levenshtein;

import java.util.LinkedList;
import java.util.List;


/**
 * Represents a Levenshtein distance algorithm instance.
 * Used to store information about things such as action
 * costs and similar.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class LevenshteinDistance {

    private final int substitutionCost;
    private final int insertionCost;
    private final int deletionCost;


    /**
     * Initiates an instance of the object with all the costs
     * set to 1
     */
    public LevenshteinDistance(){
        this(1, 1, 1);
    }


    /**
     * Initiates an instance of the object with the costs
     * as provided in the constructor.
     *
     * @param substitutionCost the cost of a substitution
     * @param insertionCost the cost of an insertion
     * @param deletionCost the cost of a deletion
     */
    public LevenshteinDistance(int substitutionCost, int insertionCost, int deletionCost){
        this.substitutionCost = substitutionCost;
        this.insertionCost = insertionCost;
        this.deletionCost = deletionCost;
    }


    /**
     * Calculate the difference between two sets of strings.
     *
     * @param list1 The target list of strings
     * @param list2 The list of strings to compare with
     * @return An instance of LevenshteinResult containing the results of the function
     */
    public LevenshteinResult difference(String[] list1, String[] list2) {
        int[][] table = new int[list1.length + 1][list2.length + 1];

        for(int x = 0; x < list1.length + 1; ++x){
            table[x][0] = x;
        }
        for(int y = 0; y < list2.length + 1; ++y){
            table[0][y] = y;
        }

        for(int x = 1; x < list1.length + 1; ++x){
            for(int y = 1; y < list2.length + 1; ++y) {
                if(list1[x-1].equals(list2[y-1]))
                    table[x][y] = table[x-1][y-1];
                else {
                    table[x][y] = Math.min(
                            Math.min(
                                table[x - 1][y] + insertionCost,
                                table[x][y - 1] + deletionCost),
                            table[x - 1][y - 1] + substitutionCost);
                }
            }
        }

        List<LevenshteinAction> actions = new LinkedList<>();
        int x = list1.length;
        int y = list2.length;
        while(x > 0 || y > 0){

            //
            int left = y > 0 ? table[x][y-1] + deletionCost : Integer.MAX_VALUE;
            int up = x > 0 ? table[x-1][y] + insertionCost : Integer.MAX_VALUE;

            // Diagonal
            int sub = x > 0 && y > 0 ? table[x-1][y-1] + substitutionCost : Integer.MAX_VALUE;
            // Equal cost
            int d =  x > 0 && y > 0 && table[x-1][y-1] == table[x][y] ? table[x][y] : Integer.MAX_VALUE;

            // The diagonal is the smallest
            if(d < up && d < left && d < sub){
                actions.add(0, LevenshteinAction.NOTHING);
                y--;
                x--;
            }

            else if(sub <= left && sub <= up){
                actions.add(0, LevenshteinAction.SUBSTITUTION);
                y--;
                x--;
            }

            // Otherwise if left is lower, we had a deletion
            else if(left <= up){
                actions.add(0, LevenshteinAction.DELETION);
                y--;
            }

            // Otherwise we had an insertion
            else {
                actions.add(0, LevenshteinAction.INSERTION);
                x--;
            }
        }

        return new LevenshteinResult(table[list1.length][list2.length], actions, list1, list2);
    }
}
