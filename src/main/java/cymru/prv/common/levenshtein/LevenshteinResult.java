package cymru.prv.common.levenshtein;

import cymru.prv.common.cli.Colors;

import java.util.LinkedList;
import java.util.List;


/**
 * Contains the results from a comparison
 * using the Levenshtein distance algorithm
 *
 * @see LevenshteinDistance
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class LevenshteinResult {

    private final String[] list1;
    private final String[] list2;
    private final int distance;
    private final List<LevenshteinAction> actions;


    /**
     * Creates a new instance of the object with the
     * parameters provided. Contains information about
     * the result of a levenshtein comparison,
     *
     * @param distance The distance between the two strings
     * @param actions The resulting actions
     * @param list1 The target string list
     * @param list2 The other string list
     */
    LevenshteinResult(int distance, List<LevenshteinAction> actions, String[] list1, String[] list2) {
        this.distance = distance;
        this.actions = actions;
        this.list1 = list1;
        this.list2 = list2;
    }


    /**
     * Returns the calculated cost between the two strings
     *
     * @return the cost between the two strings
     */
    public int getDistance() {
        return distance;
    }


    /**
     * Returns a list of actions needed to turn the
     * second string list into the target string list.
     *
     * @return a list of Levenshtein actions
     */
    public List<LevenshteinAction> getActions() {
        return actions;
    }


    /**
     * Returns a list of all common tokens between the two string lists.
     *
     * @return a list of common tokens
     */
    public List<String> getCommon(){
        List<String> strings = new LinkedList<>();
        int index = 0;
        for(LevenshteinAction action : actions){
            if (action == LevenshteinAction.NOTHING)
                strings.add(list1[index]);
            if(action != LevenshteinAction.DELETION)
                index++;
        }
        return strings;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        int i1 = 0;
        int i2 = 0;
        for (int index = 0; index < actions.size(); ++index) {
            var action = actions.get(index);
            if (index != 0)
                sb.append(" ");
            if (action == LevenshteinAction.NOTHING) {
                sb.append(list1[i1]);
                i1++;
                i2++;
            } else if (action == LevenshteinAction.SUBSTITUTION) {
                sb.append(Colors.YELLOW).append(list1[i1]).append("|").append(list2[i2]).append(Colors.RESET);
                i1++;
                i2++;
            } else if (action == LevenshteinAction.DELETION) {
                sb.append(Colors.RED).append(list2[i2]).append(Colors.RESET);
                i2++;
            } else {
                sb.append(Colors.GREEN).append(list1[i1]).append(Colors.RESET);
                i1++;
            }
        }
        return sb.toString();
    }


}
