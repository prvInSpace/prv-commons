package cymru.prv.common.classification;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;


/**
 * Represents a collection of classification maps
 *
 * @author Preben Vangberg
 * @since 1.0.0
 *
 * @see ClassificationMap
 *
 * @param <K> The type of the keys
 * @param <T> The type of the classification map data
 */
public class ClassificationMapCollection <K, T> {

    protected final Map<K, ClassificationMap<T>> collection = new ConcurrentHashMap<>();


    /**
     * Increments the data in the classification map with
     * the given key.
     *
     * @param key The key of the classification map
     * @param expected The expected classification
     * @param actual The actual classification
     */
    public void increment(K key, T expected, T actual){
        collection.putIfAbsent(key, new ClassificationMap<>());
        collection.get(key).increment(expected, actual);
    }


    /**
     * Returns the classification map with the given key
     * if it exists.
     *
     * @param key The key to search for
     * @return the classification map with the key
     */
    public ClassificationMap<T> getClassificationMap(K key){
        return collection.get(key);
    }


    /**
     * Returns the set of keys in the collection
     *
     * @return The keys of all the classification maps
     */
    public Set<K> getKeys(){
        return collection.keySet();
    }


    /**
     * Returns a boolean value indicating whether the given
     * key exists or not.
     *
     * @param key the key to search for
     * @return true if the key exists, false otherwise
     */
    public boolean hasKey(K key){
        return collection.containsKey(key);
    }


    /**
     * Returns a collections of all the classification maps
     * in the dataset.
     *
     * @return a collection of classification maps
     */
    public Collection<ClassificationMap<T>> getClassificationMaps(){
        return collection.values();
    }

}
