package cymru.prv.common.classification;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * Represents a classification map.
 * Used to store information about the number
 * of expected object and the actual class.
 *
 * The class is tread-safe.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 *
 * @param <T> The class to be used as key
 */
public class ClassificationMap <T> {

    protected final Map<T, Map<T, AtomicInteger>> data = new ConcurrentHashMap<>();

    /**
     * Increments number in the cell with the two
     * keys. Will create the cell if it does not
     * exist.
     *
     * @param expected The expected class
     * @param actual The actual class
     */
    public void increment(T expected, T actual){
        data.putIfAbsent(expected, new HashMap<>());
        data.get(expected).putIfAbsent(actual, new AtomicInteger(0));
        data.get(expected).get(actual).incrementAndGet();
    }


    /**
     * Fetches the number in the cell with the two
     * keys. If the cell does not exist it will return
     * 0.
     *
     * @param expected The expected class
     * @param actual The actual class
     * @return The data at in the cell if it exist, 0 otherwise
     */
    public int getData(T expected, T actual){
        if(!data.containsKey(expected))
            return 0;
        var expectedMap = data.get(expected);
        if(!expectedMap.containsKey(actual))
            return 0;
        return expectedMap.get(actual).get();
    }

}
