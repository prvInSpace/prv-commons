# Prv's Common Library

[![pipeline status](https://gitlab.com/prvInSpace/prv-commons/badges/master/pipeline.svg)](https://gitlab.com/prvInSpace/prv-commons/-/commits/master) 
[![coverage report](https://gitlab.com/prvInSpace/prv-commons/badges/master/coverage.svg)](https://gitlab.com/prvInSpace/prv-commons/-/commits/master) 
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)


Prv Commons is a small library that contains several small bits of reusable code that has been developed over time.

## Importing using Gradle

The latest version of the library is hosted through the package repository here on Gitlab.
That means that you are able to import the library using several different build tools.

In the list of repositories in your build.gradle file add the following repository:
```
repositories {
    maven {
        url "https://gitlab.com/api/v4/projects/25560606/packages/maven"
        name "GitLab"
    }
}
```
And then in the list of dependencies add a reference to the library like this:
```
dependencies {
    implementation 'cymru.prv:commons:1.0-SNAPSHOT'
}
```

## Maintainer

* Preben Vangberg &lt;prv@aber.ac.uk&gt;
